(ns stagehand.app
  "Server Inventory Management"
  (:require [ring.adapter.jetty :as jetty]
            [ring.util.response :refer [response content-type created status]]
            [clojure.pprint :refer [pprint]]
            [reitit.core :as r]
            [reitit.ring :as ring]
            [stagehand.queries :as queries]
            [next.jdbc :as jdbc]
            [cheshire.core :as json]
            [muuntaja.middleware :as mw]
            [muuntaja.core :as m]
            [reitit.ring.middleware.exception :as exception]
            [clojure.stacktrace :refer [print-stack-trace]]))

(def db {:dbtype "sqlite" :dbname "stagehand.db"})
(def ds (jdbc/get-datasource db))

(def muuntaja (m/create))

(defn handler [message exception request]
  (-> {:message message
       :exception (.getClass exception)
       :data (ex-data exception)
       :uri (:uri request)}
      (response)
      (status 500)))

(defn list-servers [_]
  (-> (response (queries/list-all-servers ds))))

(defn create-server [request]
  (let [new-id (random-uuid)
        location (format "/api/servers/%s" new-id)]
    (->> (queries/create-server ds (assoc (:body-params request) :id new-id))
         (created location))))

(defn dump [request]
  (-> (response (with-out-str (pprint request)))
      (content-type "text/plain")))

(defn not-found [request]
  {:status 404
   :headers {"Content-Type" "text/plain"}
   :body (str "Not Found: " (:uri request))})

#_(defn router []
  (ring/router
    [["/api" {:middleware []}
      ["/servers" {:get {:handler #'list-servers}
                   :post {:handler #'create-server}}]]
     ["/dump" {:handler #'dump}]]
    {:data
     {:middleware
      [[mw/wrap-format-negotiate muuntaja]
       [mw/wrap-format-response muuntaja]
       [exception/exception-middleware]
       [mw/wrap-format-request muuntaja]
       #_[mw/wrap-params] ;;  slow and not recommended
       ]}
     }))

(defn router []
  (ring/ring-handler
    (ring/router
      [["/api"
        ["/servers" {:get {:handler #'list-servers}
                     :post {:handler #'create-server}}]]
       ["/ping" (constantly {:status 200, :body ""})]
       ["/pong" (constantly {:status 200, :body ""})]
       ["/ident" (fn [req] (response (with-out-str (pprint req))))]])
    (ring/routes
      (ring/redirect-trailing-slash-handler)
      (ring/create-resource-handler {:path "/"})
      (ring/create-default-handler))
    {:middleware [[mw/wrap-format-negotiate muuntaja]
                  [mw/wrap-format-response muuntaja]
                  [exception/exception-middleware]
                  [mw/wrap-format-request muuntaja]]}))

(defonce server (atom nil))

(def app (ring/reloading-ring-handler #'router))

(defn start! [opts]
  (reset! server
   (jetty/run-jetty (fn [r] (app r)) opts)))

(defn stop! []
  (when-some [s @server]
    (.stop s)
    (reset! server nil)))

(defn -main
  "Invoke me with clojure -M -m stagehand.app"
  [& _args]
  (start! {:port 3000 :join? true}))

(comment
  (app {:uri "/"})

  (start! {:port 3000 :join? false})

  (stop!)

  (queries/get-server-sqlvec {:id "123"})
  (queries/get-server ds {:id "123"})

  (queries/create-server ds {:id "321" :hostname "dingo" :ip_address "127.1" :group_id nil})

  (queries/get-servers-sqlvec)
  (queries/list-all-servers ds)

  (let [new-server {:hostname "repl-server"
                    :group_id :nil
                    :ip_address "10.0.0.12"}]
    (-> 
      (app
        {:uri "/api/servers"
         :request-method :post
         :headers {"content-type" "application/json"}
         :body (json/generate-string new-server)})
      :body
      slurp))

; [{:id "123", :hostname "lappy", :ip_address "127.0.0.1", :group_id nil}
;  {:id "321", :hostname "dingo", :ip_address "127.1", :group_id nil}]

  *e)
