-- :name create-group :i!
/* :doc
Create a new group
{:id "uuid", :name "group_name"}
*/
INSERT INTO groups (id, name)
VALUES (:id, :name);

-- :name get-group :? :one
/* :doc
Get a single group by id
{:id "uuid"}
*/
SELECT id, name
FROM groups
WHERE id = :id;

-- :name update-group :!
/* :doc
Update a group's name by id
{:id "uuid", :name "new_group_name"}
*/
UPDATE groups
SET name = :name
WHERE id = :id;

-- :name delete-group :!
/* :doc
Delete a group by id
{:id "uuid"}
*/
DELETE FROM groups
WHERE id = :id;


-- :name create-server :i!
/* :doc
Create a new server
{:id "uuid", :hostname "server_hostname", :ip_address "192.168.0.1", :group_id "uuid"}
*/
INSERT INTO servers (id, hostname, ip_address, group_id)
VALUES (:id, :hostname, :ip_address, :group_id);

-- :name get-server :? :one
/* :doc
Get a single server by id
{:id "uuid"}
*/
SELECT id, hostname, ip_address, group_id
FROM servers
WHERE id = :id;

-- :name list-all-servers :? :many
/* :doc
Get all servers
*/
SELECT id, hostname, ip_address, group_id
FROM servers;

-- :name update-server :!
/* :doc
Update a server's hostname, ip_address, and group_id by id
{:id "uuid", :hostname "new_hostname", :ip_address "192.168.0.2", :group_id "uuid"}
*/
UPDATE servers
SET hostname = :hostname,
    ip_address = :ip_address,
    group_id = :group_id
WHERE id = :id;

-- :name delete-server :!
/* :doc
Delete a server by id
{:id "uuid"}
*/
DELETE FROM servers
WHERE id = :id;


-- :name create-tag :i!
/* :doc
Create a new tag
{:id "uuid", :name "tag_name"}
*/
INSERT INTO tags (id, name)
VALUES (:id, :name);

-- :name get-tag :? :one
/* :doc
Get a single tag by id
{:id "uuid"}
*/
SELECT id, name
FROM tags
WHERE id = :id;

-- :name update-tag :!
/* :doc
Update a tag's name by id
{:id "uuid", :name "new_tag_name"}
*/
UPDATE tags
SET name = :name
WHERE id = :id;

-- :name delete-tag :!
/* :doc
Delete a tag by id
{:id "uuid"}
*/
DELETE FROM tags
WHERE id = :id;


-- :name add-tag-to-server :i!
/* :doc
Add a tag to a server
{:server_id "uuid", :tag_id "uuid"}
*/
INSERT INTO servers_tags (server_id, tag_id)
VALUES (:server_id, :tag_id);

-- :name remove-tag-from-server :!
/* :doc
Remove a tag from a server
{:server_id "uuid", :tag_id "uuid"}
*/
DELETE FROM servers_tags
WHERE server_id = :server_id
AND tag_id = :tag_id;
